# Users Microservice
A microservice for managing users and user preferences for use within CACAO and other related services. The architecture of this microservice uses the ports/adapters or hexagonal architecture.

# Usage

There multiple ways to run the users microservice:

1. vscode

    You will need to install [Google Cloud Code](https://marketplace.visualstudio.com/items?itemName=GoogleCloudTools.cloudcode) extension and [skaffold](https://skaffold.dev/) beforehand.Once you import the users microservice into your workspace, vscode should recognize the launch.json in .vscode directory. 

2. skaffold

    While in the `users-microservice` directory, you can use `skaffold dev` or `skaffold run`, depending on your poison.

3. kubernetes apply
* `kubectl apply -f kubernetes-manifests/supplemental/users-configmap.yaml` # if those values are not already defined
* `kubectl apply -f kubernetes-manifests/users.yaml`

4. cli

    You can run the users microservice via command line, simply by running the command:

    ```go run main.go```


# Environment Variables

You can configure the microservice using environment variables

| Environment Variable | Description | Default |
|--- |--- |--- |
| USERSMS_DB_URI | mongo db uri | mongodb://localhost:27017 |
| USERSMS_DB_NAME | mongo database name | users |
| USERSMS_CLUSTER_ID | nats streaming cluster id | cacao-cluster |
| USERSMS_NATS_URL | nats url | nats://nats:4222 |
| USERSMS_LOG_LEVEL | debug level, either one of "debug", "trace", "info" | debug | 
| VSCODE_DEBUG_SLEEP_SECONDS | used to delay application startup for vscode to attach | 0 |

# Directory Layout
Dockerfile - main docker file for the user container
main.go - main executable for the users microservice
skaffold.yaml - main skaffold configuration

.vscode/ - vscode debug configuration

adapters/ - contains adapters, or the implementations to ports

constants/ - contains constants or defaults

domain/ - contains the central domain object

kubernetes-manifests/ - contains relevant kubernetes manifests
    skaffold/ - base skaffold kubernetes manifests (used for skaffold service templating)
    supplemental/ - supplemental kubernetes manifests if apply resources manually e.g. configmap variables

scripts/ - contains helper scripts

types/ - contains common types used across the users microservice