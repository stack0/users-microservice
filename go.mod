module users-microservice

go 1.16

require (
	github.com/cloudevents/sdk-go/v2 v2.4.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats.go v1.11.0
	github.com/nats-io/stan.go v0.9.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/cyverse/cacao-common v0.0.0-20220519222211-3879b7004ff8
	go.mongodb.org/mongo-driver v1.5.2
)
